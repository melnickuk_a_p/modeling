#include "../include/Adapter.hpp"

IAdapter::IAdapter(std::unique_ptr<IReader>& reader) : _reader(reader.release())
{};

Sp3Adapter::Sp3Adapter(std::unique_ptr<IReader>& reader) : IAdapter(reader)
{}

RinexAdapter::RinexAdapter(std::unique_ptr<IReader>& reader) : IAdapter(reader)
{}

map_sat Sp3Adapter::read(const std::string& filename){
    _reader->read(filename);
    std::any buf = _reader->getData();
    frm::sp3_t data = std::any_cast<frm::sp3_t>(buf);

    map_sat output;
    for(int j{}; j < data.nSats; ++j){

        std::vector<double> x;
        std::vector<double> y;
        std::vector<double> z;
        std::vector<double> t;
        for(int i{}; i < data.chunks.size(); ++i){
            x.push_back(data.chunks[i].sats[j].x);
            y.push_back(data.chunks[i].sats[j].y);
            z.push_back(data.chunks[i].sats[j].z);
            t.push_back( (data.chunks[i].t.dStart - data.chunks[0].t.dStart)*86400.0 + data.chunks[i].t.sStart + data.chunks[i].t.minStart*60 + data.chunks[i].t.hStart*3600);
        }
        output[std::string((char*)data.chunks[0].sats[j].id)] = std::vector<std::vector<double>>{x,y,z,t};
    }

    return output;
};

map_sat RinexAdapter::read(const std::string& filename){
    _reader->read(filename);
    std::any buf = _reader->getData();
    frm::rinex_t data = std::any_cast<frm::rinex_t>(buf);

    std::vector<std::string> ids;
    for(int i{}; i < data.states.size(); ++i){
        if(std::find(ids.begin(), ids.end(), data.states[i].id) == ids.end()){
            ids.push_back(data.states[i].id);
        }
    }

    map_sat output;
    for(const auto& id : ids){
        
        std::vector<double> x;
        std::vector<double> y;
        std::vector<double> z;
        std::vector<double> t;
    
        for(int i{}; i < data.states.size(); ++i){
            if(data.states[i].id == id){
                x.push_back(data.states[i].x);
                y.push_back(data.states[i].y);
                z.push_back(data.states[i].z);
                t.push_back((data.states[i].t.dStart - data.states[0].t.dStart)*86400.0 + data.states[i].t.sStart + data.states[i].t.minStart*60 + data.states[i].t.hStart*3600);
            }
        }
        output[id] = std::vector<std::vector<double>>{x,y,z,t};
    }

    return output;
}