#include "../include/Input.hpp"

std::vector<std::string> Input::tryParse() noexcept
{
	if (_buffer.empty())
		return std::vector<std::string>();

	//reads words in lines
	std::vector<std::string> words;
	std::string tmpStr;
	for (const auto& c : _buffer) {
		if (delimFunc(c)) {
			words.push_back(tmpStr);
			tmpStr.clear();
			continue;
		}
		tmpStr += c;
	}
	words.push_back(tmpStr);

	//check empty command
	if(!words.empty())
		return words;
}

void Input::setDelimFunc(std::function<bool(char)> newFunc) noexcept
{
	delimFunc = std::move(newFunc);
}

std::istream& operator>>(std::istream& in, Input& input) {
	if (!input._buffer.empty())
		input._buffer.clear();

	std::getline(in, input._buffer);
	return in;
}
