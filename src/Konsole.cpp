
#include "../include/Konsole.hpp"

void Konsole::usage() const noexcept
{
	std::cout << "Help page of commands that can usage in this programm:\n";

	int maxLength{};
	for (auto command : _fabComm->getArrayCommand()) {
		if (command->getName().length() > maxLength)
			maxLength = (command->getName()+"\t").length();
	}

	auto printTabs = [](int nTab) {
		int count{};
		while (count < nTab) {
			printf("\t");
			++count;
		}
	};

	for (auto command : _fabComm->getArrayCommand()) {
		std::string tmpStr = command->getName() + "\t";
		std::cout << tmpStr;
		
		int nTab = (maxLength / 4) - (maxLength % 4 == 0);
		int currentTab = ((maxLength - tmpStr.length()) / 4) - ((maxLength - tmpStr.length()) % 4 == 0);
		printTabs(currentTab);

		std::cout << command->getDescription()[0] << "\n";
		for (int idx = 1; idx < command->getDescription().size(); ++idx) {
			printTabs(nTab);
			std::cout << command->getDescription()[idx] << "\n";
		}
	}
}

void Konsole::header() const noexcept
{
	BLUE_TEXT_CONSOLE("This is programm version 1.0. For working with analys data movement AES\n");
}

Konsole::Konsole(const std::string& name) : _name(name), _fabComm(new FabriqueCommand)
{
//set title of konsole app
#ifdef WIN32
system((std::string("title ") + _name).c_str());
#elif UNIX
std::cout << "\033]0;" << name << "\007";
#endif
		this->header();
}

int Konsole::run() noexcept
{
	Input input;
	while (true) {
		std::cout << ">";
		std::cin >> input;

		auto splitStr = input.tryParse();
		if (splitStr.empty()) {
			continue;
		}

		auto command = _fabComm->getCommand(splitStr[0]);
		if (!command) {
			RED_TEXT_CONSOLE("WARNING: Unknown command!\n");
			this->usage();
			continue;
		}

		if (!command->check(splitStr)) {
			RED_TEXT_CONSOLE("WARNING: Error in arguments command!\n");
			this->usage();
			continue;
		}

		try {
			command->execute();
		}
		catch (std::exception& e) {
			GREEN_TEXT_CONSOLE(std::string(e.what()) + "\n");
		}
	}

	return 0;
}
