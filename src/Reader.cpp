#include "../include/Reader.hpp"

void Sp3Reader::read(const std::string& filename){
    auto f = utils::fcloser::open(filename, "r");

    //read #a
    _data.version[0] = fgetc(f.get());
    _data.version[1] = fgetc(f.get());
    _data.version[2] = '\0';
    //read P or V
    _data.dFlag = fgetc(f.get());

    //read YYYY:mm:dd:hh:min:ss
    fscanf(f.get(), "%d", &_data.startTime.yStart);
    fscanf(f.get(), "%d", &_data.startTime.mStart);
    fscanf(f.get(), "%d", &_data.startTime.dStart);
    fscanf(f.get(), "%d", &_data.startTime.hStart);
    fscanf(f.get(), "%d", &_data.startTime.minStart);
    fscanf(f.get(), "%f", &_data.startTime.sStart);

    //read epoch 97
    fscanf(f.get(), "%d", &_data.numbEpoch);

    //read __u+U IGS20 FIT  IAC
    fseek(f.get(), 1, SEEK_CUR);
    fgets(_data.method.usedData, 6, f.get());
    _data.method.usedData[6] = '\0';

    fseek(f.get(), 1, SEEK_CUR);
    fgets(_data.method.sysFrame, 6, f.get());
    _data.method.sysFrame[6] = '\0';
    
    fseek(f.get(), 1, SEEK_CUR);
    fgets(_data.method.tOrbit, 4, f.get());
    _data.method.tOrbit[4] = '\0';

    fseek(f.get(), 1, SEEK_CUR);
    fgets(_data.method.agency, 5, f.get());
    _data.method.agency[5] = '\0';

    //read GPS 2297  86400.00000000   900.00000000 60324 0.0000000000000
    fseek(f.get(), 4, SEEK_CUR);
    fscanf(f.get(), "%d", &_data.gpsInfo.week);
    fscanf(f.get(), "%f", &_data.gpsInfo.sWeek);
    fscanf(f.get(), "%f", &_data.gpsInfo.intervalEpoch);
    fscanf(f.get(), "%d", &_data.gpsInfo.MJD);
    fscanf(f.get(), "%f", &_data.gpsInfo.fracDay);

    //119 + id...
    fseek(f.get(), 5, SEEK_CUR);
    fscanf(f.get(), "%d", &_data.nSats);
    _data.info_sats.resize(_data.nSats);

    int bufLen = 70;
    char buffer[bufLen];
    int n = _data.nSats / 17;
    if(_data.nSats % 17 != 0){
        n++;
    }

    int j{};
    int nTmp = n;
    while(nTmp){
        fgets(buffer, bufLen, f.get());

        int i{};
        while(i < strlen(buffer)){
            if(buffer[i]==' ' || buffer[i]=='+' || buffer[i] == '\n' || buffer[i] == '\r'){
                ++i;
                continue;
            }

            memcpy(_data.info_sats[j].id, buffer+i, 3);
            _data.info_sats[j].id[3] = '\0';
            ++j;
            i+=3;   
        }

        --nTmp;
    }

    j = 0;
    nTmp = n;
    int buf = _data.nSats;
    while(nTmp){

        while(fgetc(f.get())=='+' || fgetc(f.get()) == '\n'){};

        if(buf-17 >= 0){
            int numb;
            for(int k{}; k < 17; ++k){
                fscanf(f.get(), "%d" , &numb);
                _data.info_sats[j].accuracy = numb;
                ++j;
            }
            buf -= 17;
        }else{
            int numb;
            for(int k{}; k < buf; ++k){
                fscanf(f.get(), "%d" , &numb);
                _data.info_sats[j].accuracy = numb;
                ++j;
            }
        }
        --nTmp;
    }

    //Comments
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get()); 
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());
    fgets(buffer, bufLen, f.get());

    //read chunks of date
    while(true){
        //pass *
        char c = fgetc(f.get());

        if(c==EOF || c == 'E'){
            break;
        }

        frm::sp3_t::time_t tmpTime{};

        unsigned uns_month, uns_day;
        fscanf(f.get(), "%d", &tmpTime.yStart);
        fscanf(f.get(), "%u", &tmpTime.mStart);
        fscanf(f.get(), "%u", &tmpTime.dStart);
        fscanf(f.get(), "%d", &tmpTime.hStart);
        fscanf(f.get(), "%d", &tmpTime.minStart);
        fscanf(f.get(), "%f", &tmpTime.sStart);

        std::vector<frm::sp3_t::state_sat> tmp_vec;
        for(int k{}; k < _data.nSats; ++k){
            fseek(f.get(), 2, SEEK_CUR);
            frm::sp3_t::state_sat tmpState;

            tmpState.flag = fgetc(f.get());
            tmpState.id[0] = fgetc(f.get());
            tmpState.id[1] = fgetc(f.get());
            tmpState.id[2] = fgetc(f.get());
            tmpState.id[3] = '\0';

            fscanf(f.get(), "%f" , &tmpState.x);
            fscanf(f.get(), "%f" , &tmpState.y);
            fscanf(f.get(), "%f" , &tmpState.z);
            fscanf(f.get(), "%f" , &tmpState.clock);

            tmp_vec.push_back(tmpState);
        }

        _data.chunks.push_back(frm::sp3_t::chunk_t{.t = tmpTime, .sats = tmp_vec});
        fseek(f.get(), 2, SEEK_CUR);
    }
};

std::any Sp3Reader::getData() const noexcept{
        return _data;
}

void RinexReader::read(const std::string& filename){
    auto f = utils::fcloser::open(filename, "r");

    uint8_t buflen = 255;
    char buffer[buflen];

    //Pass the header
    fgets(buffer, buflen, f.get());
    fgets(buffer, buflen, f.get());
    fgets(buffer, buflen, f.get());
    fgets(buffer, buflen, f.get());
    
    while(true){
        frm::rinex_t::state_sat tmpState;

        if(!utils::fgetstring(f.get(), tmpState.id)){
            break;
        }

        fscanf(f.get(), "%d", &tmpState.t.yStart);
        fscanf(f.get(), "%u", &tmpState.t.mStart);
        fscanf(f.get(), "%u", &tmpState.t.dStart);
        fscanf(f.get(), "%d", &tmpState.t.hStart);
        fscanf(f.get(), "%d", &tmpState.t.minStart);
        fscanf(f.get(), "%f", &tmpState.t.sStart);

        utils::fgetfloatd(f.get(), &tmpState.tauN);
        utils::fgetfloatd(f.get(), &tmpState.gammaN);
        utils::fgetfloatd(f.get(), &tmpState.frameTime);
        fseek(f.get(), 2, SEEK_CUR);
        utils::fgetfloatd(f.get(), &tmpState.x);
        utils::fgetfloatd(f.get(), &tmpState.dx);
        utils::fgetfloatd(f.get(), &tmpState.ddx);
        utils::fgetfloatd(f.get(), &tmpState.health);
        fseek(f.get(), 2, SEEK_CUR);
        utils::fgetfloatd(f.get(), &tmpState.y);
        utils::fgetfloatd(f.get(), &tmpState.dy);
        utils::fgetfloatd(f.get(), &tmpState.ddy);
        utils::fgetfloatd(f.get(), &tmpState.freqNumber);
        fseek(f.get(), 2, SEEK_CUR);
        utils::fgetfloatd(f.get(), &tmpState.z);
        utils::fgetfloatd(f.get(), &tmpState.dz);
        utils::fgetfloatd(f.get(), &tmpState.ddz);
        utils::fgetfloatd(f.get(), &tmpState.age);
        fseek(f.get(), 1, SEEK_CUR);

        _data.states.push_back(tmpState);
    }

    //for(const auto state : _data.states){
    //    std::cout << state.id << " " << (int)state.t.yStart << " " << (int)state.t.mStart << " " << (int)state.t.dStart << " "
    //    << (int) state.t.hStart << " " << (int)state.t.minStart << " " << (int)state.t.sStart << " "
    //    << state.tauN << " " << state.gammaN << " " << state.frameTime << "\n";

    //    std::cout << state.x << "  " << state.dx << " " << state.ddx << " " << state.health << "\n";
    //    std::cout << state.y << "  " << state.dy << " " << state.ddy << " " << state.freqNumber << "\n";
    //    std::cout << state.z << "  " << state.dz << " " << state.ddz << " " << state.age << "\n";
    //}
};

std::any RinexReader::getData() const noexcept{
    return _data;
}