﻿#pragma once
#include <math.h>
#include <fstream>
#include <iomanip>
#include "funcm.hpp"
#include "quartenion.hpp"

class model_t {
protected:
	Matrix<long double> res;
	long double sample_inc, t0, t1;
	Vector<long double> x0;
public:
	model_t(const Vector< long double>& vec,long double t0,long double t1,long double inc);

	void load_res2file(const char* filename);
	double get_t0() const noexcept { return t0; };
	double get_t1() const noexcept { return t1; };
	double get_step() const noexcept { return sample_inc; };
	Vector<long double> get_init() const noexcept { return x0; };
	Matrix<long double> get_result() const noexcept { return res; };

	virtual void add_result(const Vector<long double>& X, double t);
	virtual Vector<long double> get_right(const Vector<long double>& X, long double t) const;
};