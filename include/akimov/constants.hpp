﻿#pragma once

namespace math_const {
	constexpr double π = 3.14159265358979323846;
	constexpr double e = 2.718281828459045235360;
}

namespace physic_const {
	constexpr double μ = 398'600.4418e9;
	constexpr double Re = 6'378'136;
	constexpr double Ω = 7.292115e-5;
}
