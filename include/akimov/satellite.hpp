﻿#pragma once

#include "model.hpp"
#include "gravity.hpp"

class sattelite_t : public model_t {
private:
	std::shared_ptr<gravity_t> gm;
public:
	sattelite_t(const Vector<long double>& X, double t0, double t1, double inc, std::shared_ptr<gravity_t> grav_model);

	Vector<long double> get_right(const Vector<long double>& X,long double t) const override;
};
