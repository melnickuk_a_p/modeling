#pragma once
#include "gravity.hpp"
#include "../IPI_RAS/ephaccess.h"

class GravDecorator : public gravity_t{
private:
	std::unique_ptr<gravity_t> _component;
protected:
	Vector<long double> callComponentGravity(const Vector<long double>& X, double t){
		if(_component){
			return _component->get_acceleration(X, t);
		}
	}
public:
	GravDecorator() = delete;

	GravDecorator(std::unique_ptr<gravity_t> component) : _component(std::move(component)) {};
};

class MoonNormalGravity : public GravDecorator{
private:
    EphAccess* _eph;
    int _object;
    int _reference;
public:
    MoonNormalGravity(std::unique_ptr<gravity_t> component)
        :
        GravDecorator(std::move(component))
    {
        _eph = ephCreate();

        std::string object = "moon";
        std::string reference = "earth";

        ephLoadFile(_eph, "EPM/epm2021.bsp");
        ephSetDistanceUnits(_eph, EPH_KM);
        ephSetTimeUnits(_eph, EPH_SEC);

        _object = ephObjectByName(object.c_str());
        _reference = ephObjectByName(reference.c_str());
    }

    Vector<long double> get_acceleration(const Vector<long double>& X, double t) override{
        auto g_n = callComponentGravity(X, t);
        
        double pos[3];
        ephCalculateRectangular(_eph, _object, _reference, t / 86400.0, 0.0, pos, NULL);
        Vector<long double> r_moon({pos[0]*1000, pos[1]*1000, pos[2]*1000});
        auto r = r_moon - X;

        g_n = g_n + 4902.8*1e9* (r / pow(r.length(), 3) - r_moon / pow(r_moon.length(), 3));

        return g_n;
    }

    ~MoonNormalGravity(){
        ephDestroy(_eph);
    }

};

class SunNormalGravity : public GravDecorator{
private:
    EphAccess* _eph;
    int _object;
    int _reference;
public:
    SunNormalGravity(std::unique_ptr<gravity_t> component)
        :
        GravDecorator(std::move(component))
    {
        _eph = ephCreate();

        std::string object = "sun";
        std::string reference = "earth";

        ephLoadFile(_eph, "EPM/epm2021.bsp");
        ephSetDistanceUnits(_eph, EPH_KM);
        ephSetTimeUnits(_eph, EPH_SEC);

        _object = ephObjectByName(object.c_str());
        _reference = ephObjectByName(reference.c_str());
    }

    Vector<long double> get_acceleration(const Vector<long double>& X, double t) override{
        auto g_n = callComponentGravity(X, t);
        
        double pos[3];
        ephCalculateRectangular(_eph, _object, _reference, t/86400.0, 0.0, pos, NULL);
        Vector<long double> r_sun({pos[0]*1000, pos[1]*1000, pos[2]*1000});
        auto r = r_sun - X;

        g_n = g_n +  1.327124400419394e20 * (r/pow(r.length(),3) - r_sun/pow(r_sun.length(),3));

        return g_n;
    }

    ~SunNormalGravity(){
        ephDestroy(_eph);
    }

};

class MoonSunNormalGravity : public GravDecorator{
private:
    EphAccess* _eph;
    int _objectMoon, _objectSun;
    int _reference;
public:
    MoonSunNormalGravity(std::unique_ptr<gravity_t> component)
        :
        GravDecorator(std::move(component))
    {
        _eph = ephCreate();


        ephLoadFile(_eph, "EPM/epm2021.bsp");
        ephSetDistanceUnits(_eph, EPH_KM);
        ephSetTimeUnits(_eph, EPH_SEC);

        _objectMoon = ephObjectByName("moon");
        _objectSun = ephObjectByName("sun");
        _reference = ephObjectByName("earth");
    }

    Vector<long double> get_acceleration(const Vector<long double>& X, double t) override{
        auto g_n = callComponentGravity(X, t);
        
        double pos[3];
        ephCalculateRectangular(_eph, _objectSun, _reference, t / 86400.0, 0.0, pos, NULL);
        Vector<long double> r_sun({pos[0]*1000, pos[1]*1000, pos[2]*1000});
        auto r1 = r_sun - X;
        g_n = g_n + 1.327124400419394e20 * (r1 / pow(r1.length(), 3) - r_sun / pow(r_sun.length(), 3));

        ephCalculateRectangular(_eph, _objectMoon, _reference, t/86400.0, 0.0, pos, NULL);
        Vector<long double> r_moon({pos[0]*1000, pos[1]*1000, pos[2]*1000});
        auto r2= r_moon - X;
        g_n = g_n + 4902.8*1e9* (r2 / pow(r2.length(), 3) - r_moon / pow(r_moon.length(), 3));

        return g_n;
    }

    ~MoonSunNormalGravity(){
        ephDestroy(_eph);
    }
};