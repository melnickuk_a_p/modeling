#pragma once
#include <string>
#include <ranges>
#include <string_view>
#include "quartenion.hpp"
#include "constants.hpp"

namespace sky{
    inline Vector<long double> getInertionR(double a, double e, double i, double Omega, double omega, double nu){
        using namespace physic_const;
        
        //focal parameter
        double p = a * (1 - e * e);

        //get r
        double r = a * (1 - e * e) / (1 + e * cos(nu));
        double u = omega + nu;

        double x = r * (cos(u) * cos(Omega) - sin(u) * sin(Omega) * cos(i));
        double y = r * (cos(u) * sin(Omega) + sin(u) * cos(Omega) * cos(i));
        double z = r * sin(u) * sin(i);

        double vr = sqrt(μ / p) * e * sin(nu);
        double vτ = sqrt(μ / p) * (1 + e * cos(nu));

        double dx = vr * x / r - vτ * (sin(u) * cos(Omega) + cos(u) * sin(Omega) * cos(i));
        double dy = vr * y / r - vτ * (sin(u) * sin(Omega) - cos(u) * cos(Omega) * cos(i));
        double dz = vr * z / r + vτ * cos(u) * cos(i);

        return Vector<long double>({ x,y,z,dx,dy,dz });    
    }

    inline double getJulianDay(const std::string& str){
        std::vector<int> numbs;
        std::string delim = ":";
        for(const auto word : std::views::split(str, delim)){
            numbs.push_back(std::stoi(word.data()));
        }
        int a = (14-numbs[1])/12;
        int y = numbs[0] + 4800 - a;
        int m = numbs[1] + 12*a - 3;
        int JDN = numbs[2] + (153*m+2)/5 + 365*y + y/4 -y/100 +y/400 - 32045;

        return JDN + (numbs[3]-12)/24. + numbs[4]/1440. + numbs[5] / 86400.;
    }
}