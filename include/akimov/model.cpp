﻿#include "model.hpp"

model_t::model_t(const Vector<long double>& vec,long double t0,long double t1,long double inc) : x0(vec), sample_inc(inc), t0(t0), t1(t1) {};

void model_t::add_result(const Vector<long double>& X, double t) {
	res.push_row(X);
}

void model_t::load_res2file(const char* filename) {
	std::ofstream f(filename, std::ios::trunc);

	if (!f.is_open())
		throw std::logic_error("load res2file");
	else
		std::cout << "Create file" << '\n';

	for (uint64_t count = 0u; count < res.rows(); ++count)
		f << res.get_rows(count) << '\n';

	f.close();
}

Vector<long double> model_t::get_right(const Vector<long double>& X, long double t) const {
	Vector<long double> dX(X.dimension());

	long double mu = 0.012277471l;
	long double mu_ = 1 - mu;
	long double D1 = pow((X.at(0) + mu) * (X.at(0) + mu) + X.at(2) * X.at(2), 3.0l / 2.0l);
	long double D2 = pow((X.at(0) - mu_) * (X.at(0) - mu_) + X.at(2) * X.at(2), 3.0l / 2.0l);

	dX.at(0) = X.at(1);
	dX.at(1) = X.at(0) + 2 * X.at(3) - mu_ * (X.at(0) + mu) / D1 - mu * (X.at(0) - mu_) / D2;
	dX.at(2) = X.at(3);
	dX.at(3) = X.at(2) - 2 * X.at(1) - mu_ * X.at(2) / D1 - mu * X.at(2) / D2;

	return dX;
};
