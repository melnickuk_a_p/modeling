#pragma once
#include "Format.hpp"
#include "utils.hpp"
#include <iostream>
#include <cstring>
#include <array>
#include <any>

class IReader{
public:
    virtual void read(const std::string&) = 0;
    virtual std::any getData() const noexcept = 0;
};

class Sp3Reader : public IReader{
private:
    frm::sp3_t _data;
public:
    Sp3Reader() = default;

    /**
    * @brief Get from file ephemeris sattelites in struct sp3_t
    */
    void read(const std::string& filename) override;

    //Return the sp3_t full structure
    std::any getData() const noexcept override ;
};

class RinexReader : public IReader{
private:
    frm::rinex_t _data;
public:
    RinexReader() = default;

    /**
     *  @brief Get from file ephemeris GPS sattelites
     */
    void read(const std::string& filename) override;

    //Return the rinex_t full structure
    std::any getData() const noexcept override;
};