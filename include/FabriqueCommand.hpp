#pragma once
#include "Command.hpp"
#include <vector>

class IFabriqueCommand
{
public:
	virtual ACommand* getCommand(const std::string&) = 0;
	virtual std::vector<ACommand*>& getArrayCommand() noexcept = 0;
	virtual ~IFabriqueCommand() {};
};

class FabriqueCommand : public IFabriqueCommand {
private:
	std::vector<ACommand*> _commands;
public:
	FabriqueCommand() :
		_commands{
			new SatteliteMovementPlot(),
			new SatteliteIntegrateWithPlot(),
			new AirportIterator()
		}
	{};

	std::vector<ACommand*>& getArrayCommand() noexcept override {
		return _commands;
	}

	ACommand* getCommand(const std::string& name) override {
		for (const auto& command : _commands) {
			if (command->getName() == name)
				return command;
		}
		return nullptr;
	}

	~FabriqueCommand(){
		for (unsigned idx{}; idx < _commands.size(); ++idx)
			delete _commands[idx];
	}
};
