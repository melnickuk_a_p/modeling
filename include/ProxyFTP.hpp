#pragma once
#include "./lib/CkFtp2.h" 
#include <string>
#include <stdexcept>

class CFTP{
private:
    CkFtp2 _ftp;
public:
    CFTP() = default;

    void setup(const std::string& remote){
        _ftp.put_Hostname(remote.c_str());
        //_ftp.put_Username("");
        //_ftp.put_Password("");
        
        // Connect and login to the FTP server.
        bool success = _ftp.Connect();
        if (success != true)
            throw std::runtime_error(_ftp.lastErrorText());    
    }

    void todir(const std::string& dir){
        bool success = _ftp.ChangeRemoteDir(dir.c_str());
        if (success != true)
            throw std::runtime_error(_ftp.lastErrorText());    
    }

    void download(const std::string& local, const std::string& remote){
        // Download a file.
        bool success = _ftp.GetFile(remote.c_str(),local.c_str());
        if (success != true)
            throw std::runtime_error(_ftp.lastErrorText()); 
    }

    void disconnect(){
        _ftp.Disconnect();
    }

    ~CFTP(){
        disconnect();
    }
};