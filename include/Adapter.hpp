#pragma once
#include "Reader.hpp"
#include <algorithm>
#include <memory>
#include <unordered_map>

using map_sat = std::unordered_map<std::string, std::vector<std::vector<double>>>;

class IReader;

class IAdapter{
protected:
    std::unique_ptr<IReader> _reader;
public:
    IAdapter(std::unique_ptr<IReader>& reader);
    
    virtual map_sat read(const std::string&) = 0;
    
    virtual ~IAdapter() {};
};

class Sp3Adapter : public IAdapter{
public:
    Sp3Adapter(std::unique_ptr<IReader>& reader);
    map_sat read(const std::string& filename) override;
};

class RinexAdapter : public IAdapter{
public:
    RinexAdapter(std::unique_ptr<IReader>& reader);
    map_sat read(const std::string& filename) override;
};