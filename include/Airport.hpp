#pragma once
#include "IteratorVehicle.hpp"
#include <nlohmann/json.hpp>
#include "Vehicle.hpp"
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <string>

class Airport{
private:
    Inventar<vehicle_t> _vehicles;
public:
    Airport() noexcept = default;

    void load_info(const std::string& path){
        using json = nlohmann::json;

        std::ifstream f_in(path);
        json data = json::parse(f_in);

        auto aircrafts = data["aircrafts"];
        for(auto& aircraft : aircrafts){
            _vehicles.Add(vehicle_t{(int32_t)aircraft["id"],(double)aircraft["fuel"], (int8_t)aircraft["daysStore"], (int8_t)aircraft["currentDay"], (int16_t)aircraft["location"]});
        }

        auto transports = data["transports"];
        for(auto& transport : transports){
            _vehicles.Add(vehicle_t{(int32_t)transport["id"], (double)transport["fuel"], (int8_t)transport["daysStore"], (int8_t)transport["currentDay"], (int16_t)transport["location"], (double)transport["weight"]});
        }

        auto airliners = data["airliners"];
        for(auto& airliner : airliners){
            _vehicles.Add(vehicle_t{(int32_t)airliner["id"], (double)airliner["fuel"], (int8_t)airliner["daysStore"], (int8_t)airliner["currentDay"], (int16_t)airliner["location"], (int8_t)airliner["seats"], (int8_t)airliner["passengers"]});
        }

        auto helicopters = data["helicopters"];
        for(auto& helicopter : helicopters){
            _vehicles.Add(vehicle_t{(int32_t)helicopter["id"], (double)helicopter["fuel"], (int8_t)helicopter["daysStore"], (int8_t)helicopter["currentDay"], (int16_t)helicopter["location"], (int8_t)helicopter["missiles"], EVehicle::Helicopter});
        }

        auto fighters = data["fighters"];
        for(auto& fighter : fighters){
            _vehicles.Add(vehicle_t{(int32_t)fighter["id"], (double)fighter["fuel"], (int8_t)fighter["daysStore"], (int8_t)fighter["currentDay"], (int16_t)fighter["location"], (int8_t)fighter["missiles"], EVehicle::Fighter});
        }

        auto tankers = data["tankers"];
        for(auto tanker : tankers){
            _vehicles.Add(vehicle_t((int32_t)tanker["id"], (double)tanker["fuel"], (int8_t)tanker["daysStore"], (int8_t)tanker["currentDay"], (int16_t)tanker["location"], (int8_t)tanker["state"], (int16_t)tanker["demand_location"]));
        }

        auto buses = data["buses"];
        for(auto& bus : buses){
            _vehicles.Add(vehicle_t{(int32_t)bus["id"], (double)bus["fuel"], (int16_t)bus["location"], (int8_t)bus["seats"], (int8_t)bus["passengers"], (int16_t)bus["demand_location"]});
        }
    }

    void get_info(int8_t param, EVehicle type = EVehicle::Default){
        if(type == EVehicle::Default && param == -1){
            auto it = _vehicles.createIterator();
            for(it->First(); it->isDone(); it->Next()){
                std::cout << (int)it->Current()->_id << "\t" <<"Fuel: "<<it->Current()->_fuel << "\n";
            }
            return;
        }

        if(type != EVehicle::Default && param == -1){
            auto it = _vehicles.createIterator(type);
            for(it->First(); it->isDone(); it->Next()){
                std::cout << (int)it->Current()->_id << "\t" <<"Fuel: "<< it->Current()->_fuel << "\n";
            }
            return;    
        }

        if(param != -1){
            auto it = _vehicles.createIterator(param);
            for(it->First(); it->isDone(); it->Next()){
                std::cout << (int)it->Current()->_id << "\t";

                switch (param){
                case 0:
                    std::cout << "Day storage: " << (int) it->Current()->_amountDayOfStore << "\n";
                    break;
                case 1:
                    std::cout <<"Current Day: " << (int) it->Current()->_currentDayOfStore << "\n";
                    break;
                case 2:
                    std::cout <<"Location: "<< (int) it->Current()->_location << "\n";
                    break;
                case 3:
                    std::cout <<"Weight: "<< it->Current()->_weight << "\n";
                    break;
                case 4:
                    std::cout <<"Seats: "<< (int) it->Current()->_seats << "\n";
                    break;
                case 5:
                    std::cout <<"Passengers: "<< (int) it->Current()->_passengers << "\n";
                    break;
                case 6:
                    std::cout <<"Missiles: "<< (int) it->Current()->_missiles << "\n";
                    break;
                case 7:
                    std::cout <<"State: "<< (int) it->Current()->_state << "\n";
                    break;
                case 8:
                    std::cout <<"Demand location: " << (int) it->Current()->_demandLocation << "\n";
                    break;
                }

            }
            return; 
        }
    }
};