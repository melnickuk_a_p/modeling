#pragma once
#include <vector>
#include "Vehicle.hpp"

template<typename T, typename C>
class Iterator{
public:
    typedef typename std::vector<T>::iterator iter_type;
    Iterator(C* pData) noexcept : _pData(pData)
    {
        _it = _pData->_data.begin();
    };

    void First(){
        _pData->_data.begin();
    }

    void Next(){
        _it++;
    }

    bool isDone(){
        return (_it != _pData->_data.end());
    }

    iter_type Current(){
        return _it;
    }
    
private:
    C* _pData;
    iter_type _it;
};

template<typename T, typename C>
class IteratorVehicle{
public:
    typedef typename std::vector<T>::iterator iter_type;
    IteratorVehicle(C* pData, EVehicle type) noexcept : _pData(pData), _type(type)
    {
        this->First();
    };

    void First(){
        auto it = _pData->_data.begin();
        while(this->isDone()){
            if(it->_type == _type){
                _it = it;
                break;
            }
            ++it;
        }
    }

    void Next(){
        while(this->isDone()){
            ++_it;
            if(_it->_type == _type)
                break;
        }
    }

    bool isDone(){
        return (_it != _pData->_data.end());
    }

    iter_type Current(){
        return _it;
    }
    
private:
    C* _pData;
    iter_type _it;
    EVehicle _type;
};

template<typename T, typename C>
class IteratorParam{
public:
    typedef typename std::vector<T>::iterator iter_type;
    IteratorParam(C* pData, int8_t param) noexcept : _pData(pData), _param(param)
    {
        this->First();
    };

    void First(){
        auto it = _pData->_data.begin();
        while(this->isDone()){
            if(checkIsNotNull(it)){
                _it = it;
                break;
            }
            ++it;
        }
    }

    void Next(){
        while(this->isDone()){
            ++_it;
            if(checkIsNotNull(_it))
                break;
        }
    }

    bool isDone(){
        return (_it != _pData->_data.end());
    }

    iter_type Current(){
        return _it;
    }
private:
    //members
    C* _pData;
    iter_type _it;
    int8_t _param;

    //methods
    bool checkIsNotNull(iter_type it){
        switch (_param){
        case 0:
            if(it->_amountDayOfStore != -1)
                return true;
            break;
        case 1:
            if(it->_currentDayOfStore != -1)
                return true;
            break;
        case 2:
            if(it->_location != -1)
                return true;
            break;
        case 3:
            if(it->_weight != -1.0)
                return true;
            break;
        case 4:
            if(it->_seats != -1)
                return true;
            break;
        case 5:
            if(it->_passengers != -1)
                return true;
            break;
        case 6:
            if(it->_missiles != -1)
                return true;
            break;
        case 7:
            if(it->_state != -1)
                return true;
            break;
        case 8:
            if(it->_demandLocation != -1)
                return true;
            break;
        default:
            break;
        }

        return false;
    }
};

template<typename T>
class Inventar{
    friend class Iterator<T, Inventar>;
    friend class IteratorVehicle<T, Inventar>;
    friend class IteratorParam<T, Inventar>;
public:
    void Add(const T& a){
        _data.push_back(a);
    };

    Iterator<T, Inventar>* createIterator(){
        return new Iterator<T, Inventar>(this);
    }

    IteratorVehicle<T, Inventar>* createIterator(EVehicle type){
        return new IteratorVehicle<T, Inventar>(this, type);
    }

    IteratorParam<T, Inventar>* createIterator(int8_t param){
        return new IteratorParam<T, Inventar>(this, param);
    }

private:
    std::vector<T> _data;
};