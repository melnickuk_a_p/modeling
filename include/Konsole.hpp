#pragma once
#include <string>
#include <iostream>
#include "Modifier.hpp"
#include "Input.hpp"
#include "FabriqueCommand.hpp"
#include <unordered_map>

#define RED_TEXT_CONSOLE(TEXT)  do{                 \
    Color::Modifier red(Color::FG_RED);             \
    Color::Modifier def(Color::FG_DEFAULT);         \
    std::cout << red << TEXT << def;   \
    }while(false)

#define GREEN_TEXT_CONSOLE(TEXT)   do{               \
    Color::Modifier green(Color::FG_GREEN);         \
    Color::Modifier def(Color::FG_DEFAULT);         \
    std::cout << green << TEXT << def; \
    }while(false)

#define BLUE_TEXT_CONSOLE(TEXT)  do{               \
    Color::Modifier blue(Color::FG_BLUE);           \
    Color::Modifier def(Color::FG_DEFAULT);         \
    std::cout << blue << TEXT << def;  \
    }while(false)

class Konsole
{
private:
	//members
	std::string _name;

	//fabric commands
	IFabriqueCommand* _fabComm;

	//methods
	void usage() const noexcept;
	void header() const noexcept;
public:
	Konsole(const std::string& name);
	int run() noexcept;
};