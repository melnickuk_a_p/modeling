#pragma once
#include <string>
#include <vector>
#include "Loader.hpp"
#include "Input.hpp"
#include <nlohmann/json.hpp>
#include <fstream>
#include "akimov/constants.hpp"
#include "akimov/SkyMechanic.hpp"
#include "lib/matplotlibcpp.h"
#include "akimov/integrator.hpp"
#include "akimov/satellite.hpp"
#include "akimov/DecGrav.hpp"
#include <iomanip>
#include "Airport.hpp"

class ACommand
{
protected:
	std::string _name;
	std::vector<std::string> _args;
	std::vector<std::string> _descriptions;
public:
	ACommand(std::string name, std::vector<std::string> args, std::vector<std::string> descriptions) noexcept
		:
		_name(std::move(name)),
		_args(std::move(args)),
		_descriptions(std::move(descriptions))
	{};

	std::string getName() const noexcept {
		return _name;
	}

	std::vector<std::string> getDescription() const noexcept {
		return _descriptions;
	}

	//must be override in inherited class 
	virtual void execute() = 0;
	virtual bool check(std::vector<std::string>) = 0;
};

class SatteliteMovementPlot : public ACommand{
private:
    type_ns _typeNS;
    int _numbNS;
    std::string _date;
public:
    SatteliteMovementPlot() noexcept
        :
        ACommand(
            "plot",
            {"--type", "--number", "--date"},
            {"--type : GLONASS, GPS", "--number : GLONASS [1..26]; GPS[1..32]", "--date : Format yyyy:mm:dd"})
    {};

    void execute() override {

        CAnalysis loader;
        loader.load(_numbNS, _typeNS, _date);

        auto arr = std::move(loader.getData());

        matplotlibcpp::named_plot("X(t)",arr[3], arr[0]);
        matplotlibcpp::legend();
        matplotlibcpp::show(true);

        matplotlibcpp::named_plot("Y(t)",arr[3], arr[1]);
        matplotlibcpp::legend();
        matplotlibcpp::show(true);

        matplotlibcpp::named_plot("Z(t)",arr[3], arr[2]);
        matplotlibcpp::legend();
        matplotlibcpp::show(true);
    };

	bool check(std::vector<std::string> splitStr) override {
        auto itType = std::find(splitStr.begin(), splitStr.end(), _args[0]);

        if(itType == splitStr.end())
            return false;
        else
            ++itType;

        if(*itType=="GLONASS"){
            _typeNS = type_ns::GLONASS;
        }else if(*itType=="GPS"){
            _typeNS = type_ns::GPS;
        }else{
            return false;
        }

        auto itNumber = std::find(splitStr.begin(), splitStr.end(), _args[1]);

        if(itNumber == splitStr.end())
            return false;
        else
            ++itNumber;

        try{
            _numbNS = std::stoi(*itNumber);
        }catch(std::exception& e){
            return false;
        };

        if(_typeNS == type_ns::GPS && (_numbNS < 0 || _numbNS > 32))
            return false;
        
        if(_typeNS == type_ns::GLONASS && (_numbNS < 0 || _numbNS > 26))
            return false;

        auto itDate = std::find(splitStr.begin(), splitStr.end(), _args[2]);
        
        if(itDate == splitStr.end())
            return false;
        else
            ++itDate;

        _date = *itDate;

        return true;
    };
};

class AirportIterator : public ACommand{
private:
    int8_t _param;
    EVehicle _type;

    std::unordered_map<std::string, int8_t> _map_param{
        {"amountDay", 0},
        {"currentDay", 1},
        {"location", 2},
        {"weight", 3},
        {"seats", 4},
        {"passengers", 5},
        {"missiles", 6},
        {"state", 7},
        {"demandLocation", 8}
    };

    std::unordered_map<std::string, EVehicle> _map_type{
        {"LightAircraft", EVehicle::LightAircraft},
        {"TransportAircraft", EVehicle::TransportAircraft},
        {"Airliner", EVehicle::Airliner},
        {"Helicopter", EVehicle::Helicopter},
        {"Fighter", EVehicle::Fighter},
        {"Tanker", EVehicle::Tanker},
        {"Bus", EVehicle::Bus}
    };

public:
    AirportIterator() noexcept
        :
        ACommand(
            "it",
            {"--param", "--type"},
            {R"(--param : amountDay,
                          currentDay,
                          location,
                          weight,
                          seats,
                          passengers,
                          missiles,
                          state,
                          demandLocation)",
                
             R"(--type : LightAircraft,
                         TransportAircraft,
                         Airliner,
                         Helicopter,
                         Fighter,
                         Tanker,
                         Bus)"
            }
        )
    {};

    bool check(std::vector<std::string> splitStr) override{
        auto itParam = std::find(splitStr.begin(), splitStr.end(), _args[0]);

        if(itParam == splitStr.end())
            _param = -1;
        else{
            ++itParam;
            _param = _map_param[(*itParam)];
        }

        auto itType = std::find(splitStr.begin(), splitStr.end(), _args[1]);

        if(itType == splitStr.end())
            _type = EVehicle::Default;
        else{
            ++itType;
            _type = _map_type[(*itType)];
        }

        return true;
    }

    void execute() override{
        Airport Airport;
        Airport.load_info("air.json");
        Airport.get_info(_param, _type);
    }

};

class SatteliteIntegrateWithPlot : public ACommand {
private:
    std::string _path;
    std::shared_ptr<GravDecorator> _model = nullptr;
    bool _flag;

    const double _step = 60.0;
public:
    SatteliteIntegrateWithPlot() noexcept
        :
        ACommand(
            "integrate",
            {"--init", "--model", "--plot"},
            {"--init : path to init.json", "--model : Earth or Moon or Sun or MoonSun", "--plot : P - position and V - velocity"})
    {};

    bool check(std::vector<std::string> splitStr) override{

        auto itFile = std::find(splitStr.begin(), splitStr.end(), _args[0]);

        if(itFile == splitStr.end())
            return false;
        else
            ++itFile;
        
        _path = *itFile;

        auto itModel = std::find(splitStr.begin(), splitStr.end(), _args[1]);
        if(itModel == splitStr.end())
            return false;
        else
            ++itModel;


        if(*itModel == "Moon"){
            auto grav_model_normal = std::make_unique<normal_gravity_t>();
            _model = std::make_shared<MoonNormalGravity>(std::move(grav_model_normal));
        }

        if(*itModel == "Sun"){
            auto grav_model_normal = std::make_unique<normal_gravity_t>();
            _model = std::make_shared<SunNormalGravity>(std::move(grav_model_normal));
        }

        if(*itModel == "MoonSun"){
            auto grav_model_normal = std::make_unique<normal_gravity_t>();
            _model = std::make_shared<MoonSunNormalGravity>(std::move(grav_model_normal));
        }

        auto itFlag = std::find(splitStr.begin(), splitStr.end(), _args[2]);
        if(itFlag == splitStr.end())
            return false;
        else
            ++itFlag;

        if(*itFlag == "P");
            _flag = true;
        
        if(*itFlag == "V")
            _flag = false;

        return true;
    }

    void execute() override{
        using namespace math_const;
        using namespace physic_const;
        using json = nlohmann::json;

        std::ifstream f_in(_path);
        json data = json::parse(f_in);

        //Kepler's element of orbit sat on start epoch
        double a = data["ephemeris"]["a"]; a*=1'000;      //change to meters
        double e = data["ephemeris"]["e"];
        double i = rad(data["ephemeris"]["i"]);
        double Omega = rad(data["ephemeris"]["Omega"]);
        double omega = rad(data["ephemeris"]["omega"]);
        double nu = rad(data["ephemeris"]["nu"]);

        Vector<long double> R = sky::getInertionR(a, e, i, Omega, omega, nu);

        std::string beginJD_str = data["epoch"]["start"];
        std::string endJD_str = data["epoch"]["end"];

        double startJD = sky::getJulianDay(beginJD_str) * 86400.0;      //to sec
        double endJD = sky::getJulianDay(endJD_str) * 86400.0;          //to sec

                std::cout << std::setprecision(16) << "\n";
        std::cout << startJD << "\n";

        sattelite_t sat(R, startJD, endJD, _step, _model);
        DormandPrinceIntegrator integrator(1e-16l);
        integrator.run(sat);

        auto mtx_model = sat.get_result();
        
        //block without
        auto gnormal = std::make_shared<normal_gravity_t>();
        sattelite_t sat2(R, startJD, endJD, _step, gnormal);
        integrator.run(sat2);

        auto mtx_etalon = sat2.get_result();

        std::vector<long double> tmt;
            for(double t = startJD; t < endJD; t+=_step){
                tmt.push_back(t);
        }

        if(_flag){
            auto vecX1 = mtx_model.get_cols(0);
            auto vecX2 = mtx_etalon.get_cols(0);

            auto vecX = vecX1 - vecX2;

            auto vecY1 = mtx_model.get_cols(1);
            auto vecY2 = mtx_etalon.get_cols(1);

            auto vecY = vecY1 - vecY2;

            auto vecZ1 = mtx_model.get_cols(2);
            auto vecZ2 = mtx_etalon.get_cols(2);

            auto vecZ = vecZ1 - vecZ2;

            Vector<long double> delta_r;
            for(int idx{}; idx < vecZ.dimension(); ++idx){
                delta_r.push_back(sqrt(vecX[idx]*vecX[idx] +  vecY[idx]*vecY[idx] +  vecZ[idx]*vecZ[idx]));
            }

            matplotlibcpp::named_plot("X(t)", tmt, vecX.data());
            matplotlibcpp::legend();
            matplotlibcpp::show(true);

            matplotlibcpp::named_plot("Y(t)", tmt, vecY.data());
            matplotlibcpp::legend();
            matplotlibcpp::show(true);

            matplotlibcpp::named_plot("Z(t)", tmt, vecZ.data());
            matplotlibcpp::legend();
            matplotlibcpp::show(true);
        }else{
            auto vecVX1 = mtx_model.get_cols(3);
            auto vecVX2 = mtx_etalon.get_cols(3);

            auto vecVX = vecVX1 - vecVX2;

            auto vecVY1 = mtx_model.get_cols(4);
            auto vecVY2 = mtx_etalon.get_cols(4);

            auto vecVY = vecVY1 - vecVY2;

            auto vecVZ1 = mtx_model.get_cols(5);
            auto vecVZ2 = mtx_etalon.get_cols(5);

            auto vecVZ = vecVZ1 - vecVZ2;

            Vector<long double> delta_r;
            for(int idx{}; idx < vecVZ.dimension(); ++idx){
                delta_r.push_back(sqrt(vecVX[idx]*vecVX[idx] +  vecVY[idx]*vecVY[idx] +  vecVZ[idx]*vecVZ[idx]));
            }

            matplotlibcpp::named_plot("VX(t)", tmt, vecVX.data());
            matplotlibcpp::legend();
            matplotlibcpp::show(true);

            matplotlibcpp::named_plot("VY(t)", tmt, vecVY.data());
            matplotlibcpp::legend();
            matplotlibcpp::show(true);

            matplotlibcpp::named_plot("VZ(t)", tmt, vecVZ.data());
            matplotlibcpp::legend();
            matplotlibcpp::show(true);
        }
    };
};