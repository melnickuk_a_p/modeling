#pragma once
#include <iostream>
#include <functional>
#include <unordered_map>
#include <string>
#include <vector>
#include <optional>

class Input
{
private:
	std::string _buffer;

	std::function<bool(char)> delimFunc = [](char c) {
		return (c == '=' || c == ' ');
		};
public:
	Input() noexcept = default;

	std::vector<std::string> tryParse() noexcept;

	void setDelimFunc(std::function<bool(char)> newFunc) noexcept;

	//friend functions
	friend std::istream& operator>>(std::istream& in, Input& input);
};

