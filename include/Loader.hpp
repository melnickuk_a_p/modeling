#pragma once
#include "Adapter.hpp"
#include "Reader.hpp"
#include "ProxyFTP.hpp"
#include <filesystem>

namespace fs = std::filesystem;

enum class type_ns : uint8_t{
    GPS,
    GLONASS
};

class CAnalysis{
private:
    std::unique_ptr<IAdapter> _adapter = nullptr;
    CFTP _ftp;
    std::vector<std::vector<double>> _data;
public:
    CAnalysis() noexcept = default;

    void load(int num_ns, type_ns NS, const std::string& time){
        std::string remote = "ftp.glonass-iac.ru";
        std::string dir, filename, tmp, id;

        utils::data_t datestamp = utils::toData(time);
        const auto [day_year, numb_week, add_day] = utils::metricsTime(datestamp);

        //make 24<day_year>
        tmp = utils::itostr(day_year);
        if(day_year < 100){
            tmp = "0"+tmp;
        }
        
        //make path to file
        if(NS == type_ns::GPS){
            dir = "MCC/PRODUCTS/24"+tmp+"/final";
            //make Sta<numb_week><add_day>.sp3
            tmp = utils::itostr(numb_week);
            filename = "Sta"+tmp;
        
            tmp = utils::itostr(add_day);
            filename = filename + tmp + ".sp3";

            if(num_ns < 10){
                id = "G0"+std::to_string(num_ns);
            }else{
                id = "G"+std::to_string(num_ns);
            }
        }else if(NS == type_ns::GLONASS){
            dir = "MCC/BRDC/2024";
            
            // make Brdc<offset>.24g
            int offset = (day_year - 4)*10;
            tmp = utils::itostr(offset);
            if(offset < 100){
                tmp = "00"+tmp;
            }else if(offset < 1000){
                tmp = "0"+tmp;
            }

            filename = "Brdc"+tmp+".24g";

            if(num_ns < 10){
                id = "R0"+std::to_string(num_ns);
            }else{
                id = "R"+std::to_string(num_ns);
            }
        }

        fs::path pathToCache = "./cache";
        if(!fs::exists(pathToCache)){
            fs::create_directory("./cache");
        }
        
        fs::path pathToFile = "./cache/"+filename;
        if(!fs::exists(pathToFile)){
            _ftp.setup(remote);
            _ftp.todir(dir);
            _ftp.download(pathToFile.string(), filename);
            _ftp.disconnect();
        }

        if(type_ns::GPS == NS){
            std::unique_ptr<IReader> reader = std::make_unique<Sp3Reader>();
            _adapter = std::make_unique<Sp3Adapter>(reader);
        }else{
            std::unique_ptr<IReader> reader = std::make_unique<RinexReader>();
            _adapter = std::make_unique<RinexAdapter>(reader);
        }

        auto tmpData = _adapter->read(pathToFile.string());
        _data = std::move(tmpData[id]);
    }

    std::vector<std::vector<double>> getData() const noexcept{
        return _data;
    }
};