#pragma once
#include <cstdint>
#include <stdio.h>
#include <sstream>
#include <memory>
#include <string>
#include <tuple>

namespace utils{

    /**
     * @brief Structure consist of year, month and day 
     */
    struct data_t {
        uint16_t year;
        uint8_t month;
        uint8_t day;
    };

    /**
     * @brief Function convert text to data_t
     * @param Str format "yyyy:mm::dd"
     */
    inline data_t toData(const std::string& str){
        if(str.size() != 10){
            return {};
        }

        std::string year = str.substr(0, 4);
        int yearInt = std::stoi(year);
        
        if( yearInt < 2000 || yearInt > 2024){
            return {};
        }

        std::string month = str.substr(5, 2);
        int monthInt = std::stoi(month);

        if(monthInt < 1 || monthInt > 12){
            return {};
        }

        std::string day = str.substr(8, 2);
        int dayInt = std::stoi(day);

        if(dayInt < 1 || dayInt > 31){
            return {};
        }

        return data_t{
            .year = (uint16_t)yearInt,
            .month = (uint8_t)monthInt,
            .day = (uint8_t)dayInt
        };
    }

    /**
     * @brief Function return day_year, numb_week, add_day in time GPS
     */
    inline auto metricsTime(const data_t& datestamp){
        int day_year;
        int numb_week;
        int add_day;

        if(datestamp.month == 1){
            day_year = datestamp.day;
        }else if(datestamp.month == 2){
            day_year = datestamp.day + 31;
        }else if(datestamp.month == 3){
            if(datestamp.year % 4 == 0){
                day_year += 1;
            }
            day_year = datestamp.day + 31 + 28;
        }else{
            if(datestamp.year % 4 == 0){
                day_year += 1;
            }
            day_year = (datestamp.month)/2*31+(datestamp.month-1)/2*30-2;
            day_year += datestamp.day;
        }

        numb_week = 2295 + day_year / 7;
        add_day = day_year % 7;

        return std::tuple<int, int, int>(day_year, numb_week, add_day);
    }

    inline std::string itostr(int integer){
        std::stringstream ss;
        ss << integer;
        return ss.str();
    }

    struct fcloser{
        void operator()(FILE* fp) const{
            fclose(fp);
        };

        static auto open(const std::string& str, const std::string& mode){
            return std::unique_ptr<FILE, fcloser>(fopen(str.c_str(), mode.c_str()));
        };
    };
    
    inline bool fgetstring(FILE* f, std::string& str){
        bool begin = false;
        while(true){
            char c = fgetc(f);
            
            if(c == ' ' && !begin){
                continue;
            }else if(c==' ' && begin){
                break;
            }else if(c !=' '){
                begin = true;
            }

            if(c==EOF){
                return false;
            }

            str += c;
        }

        return true;
    }

    inline void fgetfloatd(FILE* f, double* d){
        bool begin = false;
        bool count = false;
        std::string bufStr;
        int counter;

        while(true){
            char c = fgetc(f);

            if(c==' ' && !begin){
                continue;
            }else if(c==' ' && begin){
                break;
            }else if(c!=' '){
                begin = true;
            }

            if(count && counter != 3){
                ++counter;
            }else if(count){
                break;
            }

            if(c=='D'){
                c='E';
                counter = 0;
                count = true;
            }

            bufStr += c;
        }

        *d = std::stod(bufStr);
    }

};