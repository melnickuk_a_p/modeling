#pragma once
#include <iostream>
#include <stdint.h>
#include <vector>
#include <string>

namespace frm{
    struct sp3_t{
        char version[3];
        char dFlag;

        struct time_t{
            uint16_t yStart;
            uint8_t mStart;
            uint8_t dStart;
            uint8_t hStart;
            uint8_t minStart;
            float sStart;
        } startTime;

        uint16_t numbEpoch;
        
        struct getdata_t {
            char usedData[6];
            char sysFrame[6];
            char tOrbit[4];
            char agency[5];
        } method;

        struct gps_time {
            uint16_t week;
            float sWeek;
            float intervalEpoch;
            uint32_t MJD;
            float fracDay;
        } gpsInfo;

        struct info_sat{
            char id[4];
            uint8_t accuracy;
        };
        uint16_t nSats;
        std::vector<info_sat> info_sats;

        struct base_comment{
            uint8_t line;
            uint8_t pos;
        };

        struct c_comment : public base_comment {
            char* comment;
        };

        struct f_comment : public base_comment {
            float f;
        };

        struct i_comment : public base_comment {
            int i;
        };

        struct str_comment : public base_comment{
            std::string str;
        };

        std::vector<c_comment> comments_c;
        std::vector<i_comment> comments_i;
        std::vector<f_comment> comments_f;
        std::vector<str_comment> comments_str;

        struct state_sat{
            unsigned char flag;
            unsigned char id[4];
            float x, y, z, clock;
        };

        struct chunk_t{
            time_t t;
            std::vector<state_sat> sats;
        };

        std::vector<chunk_t> chunks;

        friend std::ostream& operator<<(std::ostream& out, const frm::sp3_t& data){
            //first line
            out << data.version << data.dFlag;
            out << data.startTime.yStart << "\t" << data.startTime.dStart << "\t" << data.startTime.mStart
            <<"\t" << data.startTime.hStart << "\t" << data.startTime.minStart << "\t" << data.startTime.sStart << "\t";
            out << data.numbEpoch << " " << data.method.usedData << "  " << data.method.sysFrame << "  " << data.method.tOrbit << "  " << data.method.agency << "\n";

            //second line
            out << data.gpsInfo.week << "\t" << data.gpsInfo.intervalEpoch << "\t" << data.gpsInfo.sWeek << "\t" << data.gpsInfo.MJD << "\t" << data.gpsInfo.fracDay << "\n"; 

            //few lines
            out << data.nSats << "\n";
            for(const auto& info : data.info_sats){
                out << (int)info.accuracy << "\t" << info.id << "\n";
            }
        }
    };

    struct rinex_t{
        std::string version;
        
        struct time_t{
            uint16_t yStart;
            uint8_t mStart;
            uint8_t dStart;
            uint8_t hStart;
            uint8_t minStart;
            float sStart;
        };

        struct state_sat{
            std::string id;
            time_t t;
            double tauN;
            double gammaN;
            double frameTime;
            double x, dx, ddx, health;
            double y, dy, ddy, freqNumber;
            double z, dz, ddz, age;
        };

        std::vector<state_sat> states;
    };
}