#pragma once
#include <stdint.h>

enum class EVehicle : uint8_t {
    Default,
    Store,
    LightAircraft,
    TransportAircraft,
    Airliner,
    Helicopter,
    Fighter,
    Tanker,
    Bus
};

struct vehicle_t{
    int32_t _id;
    double _fuel = -1.0;
    EVehicle _type = EVehicle::Default;
    int8_t _amountDayOfStore = -1;
    int8_t _currentDayOfStore = -1;
    int16_t _location = -1;
    double _weight = -1.0;
    int8_t _seats = -1;
    int8_t _passengers = -1;
    int8_t _missiles = -1;
    int8_t _state = -1;
    int16_t _demandLocation = -1;

    explicit vehicle_t(int32_t id, double fuel, int8_t amount, int8_t curr, int16_t loc)
        :
        _id(id),
        _fuel(fuel),
        _amountDayOfStore(amount),
        _currentDayOfStore(curr),
        _location(loc),
        _type(EVehicle::LightAircraft)
    {};

    explicit vehicle_t(int32_t id, double fuel, int8_t amount, int8_t curr, int16_t loc, double weight)
        :
        _id(id),
        _fuel(fuel),
        _amountDayOfStore(amount),
        _currentDayOfStore(curr),
        _location(loc),
        _type(EVehicle::TransportAircraft),
        _weight(weight)
    {};

    explicit vehicle_t(int32_t id, double fuel, int8_t amount, int8_t curr, int16_t loc, int8_t seats, int8_t passengers)
        :
        _id(id),
        _fuel(fuel),
        _amountDayOfStore(amount),
        _currentDayOfStore(curr),
        _location(loc),
        _type(EVehicle::Airliner),
        _seats(seats),
        _passengers(passengers)
    {};

    explicit vehicle_t(int32_t id, double fuel, int8_t amount, int8_t curr, int16_t loc, int8_t miss, EVehicle type)
        :
        _id(id),
        _fuel(fuel),
        _amountDayOfStore(amount),
        _currentDayOfStore(curr),
        _location(loc),
        _missiles(miss),
        _type(type)
    {};

    explicit vehicle_t(int32_t id, double fuel, int8_t amount, int8_t curr, int16_t loc, int8_t state, int16_t demLoc)
        :
        _id(id),
        _fuel(fuel),
        _amountDayOfStore(amount),
        _currentDayOfStore(curr),
        _location(loc),
        _type(EVehicle::Tanker),
        _state(state),
        _demandLocation(demLoc)
    {};

    explicit vehicle_t(int32_t id, double fuel, int16_t loc, int8_t seats, int8_t passengers, int16_t demLoc)
        :
        _id(id),
        _fuel(fuel),
        _location(loc),
        _seats(seats),
        _passengers(passengers),
        _demandLocation(demLoc),
        _type(EVehicle::Bus)
    {};
};